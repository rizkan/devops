# QUESTION
1. Install docker di sebuah server EC2 ubuntu 20.04 di AWS dan install / configure AWS CLI

# ANSWER
sudo apt install apt-transport-https ca-certificates curl software-properties-common \
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - \
sudo add-apt-repository "deb \[arch=amd64\] https://download.docker.com/linux/ubuntu focal stable"

sudo apt update \
sudo apt-cache policy docker-ce
# Install docker
sudo apt install docker-ce
# Start docker service and enable after boot
sudo systemctl start docker \
sudo systemctl enable docker

# Install AWS CLI
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
unzip awscliv2.zip \
sudo ./aws/install

# Configure AWS CLI
aws configure \
AWS Access Key ID [None]: {AWSKEY} \
AWS Secret Access Key [None]: {AWSSECRETKEY} \
Default region name [None]: ap-southeast-1 \
Default output format [None]: json 


# QUESTION
2. Pertanyaan kedua ini berhubungan dengan proses CI/CD Tuliskan 1 Dockerfile config + CI/CD Pipeline YAML file. Preferably, YAML filenya untuk CI/CD Gitlab.

# ANSWER
docker-build: \
  image: docker:latest \
  stage: build \
  services: \
    - docker:dind \
  before_script: \
    - apk add --update --no-cache git \
    - 'command -v ssh-agent >/dev/null || ( apk add --update openssh )' \ 
    - eval $(ssh-agent -s) \
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - \
    - mkdir -p ~/.ssh \
    - chmod 700 ~/.ssh \
    - ssh-keyscan $VM_IPADDRESS >> ~/.ssh/known_hosts \
    - chmod 644 ~/.ssh/known_hosts \
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY \
  script: \
    - | \
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then \
        tag="" \
        echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = 'latest'" \
      else \
        tag=":$CI_COMMIT_REF_SLUG" \
        echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag" \
      fi \
    - docker build --pull -t "$CI_REGISTRY_IMAGE${tag}" . \
    - docker push "$CI_REGISTRY_IMAGE${tag}" \
  rules: \
    - if: $CI_COMMIT_BRANCH \
      exists: \
        - Dockerfile \
  script: \
    - apk add rsync \
    - apk add sshpass \
    - sshpass -p "$SSH_PASSWORD" ssh -o StrictHostKeyChecking=no root@$VM_IPADDRESS "docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY && docker run registry.gitlab.com/rizkan/devops:latest"


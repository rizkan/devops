FROM ubuntu:20.04

MAINTAINER Rizkan Abadi

RUN apt-get update \
    && apt-get install -y nginx \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && echo "daemon off;" >> /etc/nginx/nginx.conf

WORKDIR /usr/share/nginx/html
ADD index.html /usr/share/nginx/html/index.html

EXPOSE 80
CMD ["nginx"]
